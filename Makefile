all : BSNormalize.html EVEresultsAnalysis.html FractionationAnalysis.html KEGGanalysis.html TFBS_differential_binding.html TFBS_evolution.html TissueAtlasAnalysis.html dNdS_analysis.html exprsDivergencePlots.html orthoGroupFiltering.html promoterTEoverlap.html proteinComplexs.html symmetry_analyses.html upcons_TFanalysis.html 

BSNormalize.html data/BSNormalize/combExprMat.RDS : BSNormalize.Rmd data/orthoGroupFiltering/N9geneIDtbl.RDS
	Rscript -e 'rmarkdown::render("BSNormalize.Rmd")' 
EVEresultsAnalysis.html : EVEresultsAnalysis.Rmd data/BSNormalize/combExprMat.RDS data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("EVEresultsAnalysis.Rmd")' 
FractionationAnalysis.html : FractionationAnalysis.Rmd data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("FractionationAnalysis.Rmd")' 
KEGGanalysis.html data/KEGGanalysis/keggPathways data/KEGGanalysis/keggResSingle.tsv data/KEGGanalysis/keggResDuplicate.tsv data/KEGGanalysis/EVEwithKeggResDuplicate.tsv : KEGGanalysis.Rmd data/BSNormalize/combExprMat.RDS data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("KEGGanalysis.Rmd")' 
TFBS_differential_binding.html : TFBS_differential_binding.Rmd
	Rscript -e 'rmarkdown::render("TFBS_differential_binding.Rmd")' 
TFBS_evolution.html : TFBS_evolution.Rmd data/runEVE/EVEresTbl.RDS data/KEGGanalysis/keggResDuplicate.tsv
	Rscript -e 'rmarkdown::render("TFBS_evolution.Rmd")' 
TissueAtlasAnalysis.html data/TissueAtlasAnalysis/nShiftedTblComplete.RDS : TissueAtlasAnalysis.Rmd data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("TissueAtlasAnalysis.Rmd")' 
dNdS_analysis.html : dNdS_analysis.Rmd data/runEVE/EVEresTbl.RDS data/TissueAtlasAnalysis/nShiftedTblComplete.RDS
	Rscript -e 'rmarkdown::render("dNdS_analysis.Rmd")' 
exprsDivergencePlots.html : exprsDivergencePlots.Rmd data/BSNormalize/combExprMat.RDS
	Rscript -e 'rmarkdown::render("exprsDivergencePlots.Rmd")' 
orthoGroupFiltering.html data/orthoGroupFiltering/N9geneIDtbl.RDS : orthoGroupFiltering.Rmd
	Rscript -e 'rmarkdown::render("orthoGroupFiltering.Rmd")' 
promoterTEoverlap.html : promoterTEoverlap.Rmd data/TissueAtlasAnalysis/nShiftedTblComplete.RDS data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("promoterTEoverlap.Rmd")' 
proteinComplexs.html : proteinComplexs.Rmd data/runEVE/EVEresTbl.RDS data/TissueAtlasAnalysis/nShiftedTblComplete.RDS
	Rscript -e 'rmarkdown::render("proteinComplexs.Rmd")' 
symmetry_analyses.html : symmetry_analyses.Rmd data/BSNormalize/combExprMat.RDS data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("symmetry_analyses.Rmd")' 
upcons_TFanalysis.html : upcons_TFanalysis.Rmd data/runEVE/EVEresTbl.RDS
	Rscript -e 'rmarkdown::render("upcons_TFanalysis.Rmd")' 
data/runEVE/EVEresTbl.RDS : runEVE.R data/BSNormalize/combExprMat.RDS data/orthoGroupFiltering/N9geneIDtbl.RDS
	Rscript runEVE.R 